/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab4;

import java.util.Scanner;

/**
 *
 * @author Kim
 */
public class Game {
    private Table table;
    private Player player1;
    private Player player2;
    
    public Game() {
        this.player1 = new Player('O');
        this.player2 = new Player('X');
    }
    
    public void play(){
        showWelcome();
        newGame();
        while (true) {
            showTable();
            showTurn();
            inputRowCol();
            if(table.checkWin()) {
                showTable();
                showInfo();
                newGame();
            }
            if(table.checkDraw()) {
                showTable();
                showInfo();
                newGame();
            }
            table.switchPlayer();
        }
    }
    
    private void showWelcome() {
            System.out.println("Welcome to OX Game");
    }
    
    private void showTable() {
        char[][] t = table.getTable();
        for(int i=0; i<3; i++) {
            for(int j=0; j<3; j++){
                System.out.print(t[i][j] + " ");
            }
            System.out.println("");
        }
    }
    
    private void showTurn() {
        System.out.println(table.getCurrentPlayer().getSymbol()+" Turn");
    }
    
    private void showInfo() {
        System.out.println(player1);
        System.out.println(player2);
    }
    
    private void newGame() {
        table = new Table(player1, player2);
    }
    
    /// input
    private void inputRowCol() {
        Scanner sc = new Scanner(System.in);
        while(true) {
            System.out.print("Please input row col:");
            int row = sc.nextInt();
            int col = sc.nextInt();
            if (table.setRowCol(row, col)){
                break;
            }else {
                System.out.println("Invalid input. Please try again.");
            }
        }
    }
    
    
    


    private void saveWin() {
        if(player1 == table.getCurrentPlayer()) {
            player1.win();
            player2.lose();
        }else{
            player1.lose();
            player2.win();
        }
    }

    private void saveDraw() {
            player1.draw();
            player2.draw();
    }   
}
